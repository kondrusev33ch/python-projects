# Python-Projects
This is some of my applications I made at work, by myself. 
I post it here for **demonstration purposes only**.

## publish_mrf_on_worldview.py
This code is an automatization of publishing new layers in the worldview. This process was all manually, and it was so hard, especially creation of config files, almost impossible not to make a mistake somewhere, or forget something. So the main idea of this script is to do new layers publication with "one click".   
**Stack:** pexpect and linux commands.

## download_tif_and_generate_mrf.py (test version)
Script for downloading .tif files from s3ext.gptl.ru and converting them to mrf files, so you can publish these maps on the worldview.  
**Stack:** pexpect, linux commands, requests and dask.

## FTP_FY3D_DataLoader.ipynb/FTP_FY4A_DataLoader.ipynb/HTTP_GK2A_DataLoader.ipynb
These three notebooks are created with one purpose, download weather satellites data from the API (GK2A Korean) and from the FTP storage (FY3D, FY4A China).  
**Stack:** requests, dask and fsspec.
