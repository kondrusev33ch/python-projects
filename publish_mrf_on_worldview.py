"""
Автоматизация процесса публикации на WorldView

Date: 06.2022
"""


import os
import json
import pexpect
from time import sleep
from glob import glob
from datetime import datetime, timedelta

SERVER_ONEARTH = '***'
SERVER_WORLDVIEW = '***'

USER = 'administrator'
USER_SERVER_OE = f'{USER}@{SERVER_ONEARTH}'  # для удобства собрал вместе пару user/server 
USER_SERVER_WV = f'{USER}@{SERVER_WORLDVIEW}'

PASSWORD = 'admin1admin'
PASSWORD_REQUEST_OE = f"{USER_SERVER_OE}'s password: "
PASSWORD_REQUEST_WV = f"{USER_SERVER_WV}'s password: "
SUDO_PASSWORD_REQUEST = f'[sudo] password for {USER}: ' 
YES = 'Are you sure you want to continue connecting (yes/no)?'

MAIN_PATH = '/***/worldview/'
LAYERS_FILE = 'layers_list.txt'


# ------------------------------------------------------------------------------------------------------------------------------------------
class Publisher:
    def __init__(self, identifier, title, subtitle, period, description, create_new_group,
                 add_new_group_to, new_category_id, category_in, create_new_category, group, 
                 layer_group, size_x, size_y, empty_tile):
        self.layer_id = identifier
        self.title = title            # видимое название
        self.subtitle = subtitle    
        self.period = eval(period)    # с какой периодичностью будут показываться снимки
        self.description = description
        self.group = group
        self.layer_group = layer_group  
        self.size_x = size_x
        self.size_y = size_y
        self.empty_tile = empty_tile  # адрес с подложкой
        
        # Создаём новую категорию
        self.new_category_id = new_category_id
        self.create_new_category = create_new_category
        self.category_in = category_in
        self.create_nc = new_category_id and create_new_category and category_in
        
        # Создаём новую группу
        self.create_new_group = create_new_group
        self.add_new_group_to = add_new_group_to
        self.create_ng = False
        if create_new_group and add_new_group_to:
            self.group = list(self.create_new_group['measurements'].keys())[0]
            self.create_ng = True
        
        
    # Communication methods
    # ======================================================================================================================================
    def _send_scp_command_from_nfs_to_ssh_docker(self, 
                                                 command: str,  # команда которая будет исполняться 
                                                 error_message: str,  # сообщение которое будет выведено в случае неверного ответа
                                                 password_request: str,  # точная строка, котрую выдаёт консоль при запросе пароля
                                                 file_name: str,  # название файла для проверки 
                                                 pause: int=1) -> bool:
        """
        Метод нужeн для отправки команды с ***, по типу scp в докер по ssh.  
        Создаётся процесс pexpect, затем в этом процессе исполняется наша команда. 
        """
        
        with pexpect.spawnu(command) as p:  # создаём процесс и отправляем команду
            if p.expect_exact([password_request, pexpect.EOF, pexpect.TIMEOUT]) == 0:  # в ответ ожидаем password_request
                p.sendline(PASSWORD)  # отправляем пароль
                if p.expect_exact([file_name,  # название файла есть в ответе, значит команда scp сработала 
                                   pexpect.EOF,  # достигнут конец файла, что тоже может означать неправильный пароль
                                   pexpect.TIMEOUT], timeout=pause) == 0:  # ни один из ожидаемых ответов не получен
                    print('[+] Command:', command)
                    return True
                
 
            print(error_message)
            print('Error:', p.before)  
            return False
            
            
    def _send_command_ssh_onearth_scp(self,  
                                      command: str,  # команда которая будет исполняться
                                      error_message: str,  # сообщение которое будет выведено в случае неверного ответа 
                                      file_name: str,  # название файла для проверки
                                      pause: int=5) -> bool:  # пауза стоит не случайно, иногда команда не успевает исполниться
        """
        Метод нужeн для отправки команды scp из onearth, для tile-service.
        Создаём процесс pexpect, переходим в докер onearth, далее исполняем команду scp.
        
        Зачем нужно подтверждение?
            Иногда, вылетает такое предупреждение:

            The authenticity of host '***' can't be established.
            Blah blah blah ...
            Are you sure you want to continue connecting (yes/no)? 
            
            Здесь описан более простой способ решения этой проблемы
            https://stackoverflow.com/questions/3663895/ssh-the-authenticity-of-host-hostname-cant-be-established
            Но автор предупреждает что это не безопасно
            Я решил просто делать проверку для каждой команды связанной с ssh.
            
        Почему два пароля?
            Так как наша команда состоит из sudo и scp с ssh то консоль запрашивает сначала пароль для sudo а
            затем и для user@server. 
        """
        
        # Cоздаём процесс и отправляем команду для перехода в onearth
        with pexpect.spawnu(f'ssh {USER_SERVER_OE}') as ssh:
            respond = ssh.expect_exact([PASSWORD_REQUEST_OE, pexpect.EOF, pexpect.TIMEOUT, YES])
            if respond == 3:  # проверяем нужно ли подтверждение
                ssh.sendline('yes')  # отправляем подтверждение 
                ssh.expect('.*')  # ожидаем возврат любой строки
                
            if respond not in (1, 2):  # проверяем нет ли ошибок и учитываем вариант YES 
                ssh.sendline(PASSWORD)  # отправляем пароль
                if ssh.expect_exact([pexpect.EOF, pexpect.TIMEOUT], timeout=1) == 1:  # pexpect.TIMEOUT для подтверждения
                    
                    # Отправляем нашу команду 
                    ssh.sendline(command)
                    respond = ssh.expect_exact([SUDO_PASSWORD_REQUEST, pexpect.EOF, pexpect.TIMEOUT, YES])
                    if respond == 3:  # проверяем нужно ли подтверждение
                        ssh.sendline('yes')  # отправляем подтверждение 
                        ssh.expect('.*')  # ожидаем возврат любой строки
                    
                    if respond not in (1, 2):  # проверяем нет ли ошибок и учитываем вариант YES  
                        ssh.sendline(PASSWORD)  # отправляем пароль
                        respond = ssh.expect_exact([PASSWORD_REQUEST_OE, pexpect.EOF, pexpect.TIMEOUT, YES], timeout=pause)
                        if respond == 3:  # проверяем нужно ли подтверждение 
                            ssh.sendline('yes')  # отправляем подтверждение 
                            ssh.expect('.*')  # ожидаем возврат любой строки
                        
                        if respond not in (1, 2):  # проверяем нет ли ошибок и учитываем вариант YES 
                            ssh.sendline(PASSWORD)  # отправляем пароль
                            if ssh.expect_exact([file_name, pexpect.EOF, pexpect.TIMEOUT], timeout=pause) == 0:  
                                print('[+] Command:', command)
                                return True
                            
            print(error_message)
            print('Error:', ssh.before)
        return False

                
    def _send_command_ssh_onearth_status(self, 
                                         command: str,  # команда которая будет исполняться
                                         error_message: str,  # сообщение которое будет выведено в случае неверного ответа 
                                         pause: int=1) -> bool:  # пауза стоит не случайно, иногда команда не успевает исполниться
        """
        Метод нужeн для отправки односложной команды из onearth (например перезапуск или создание директории), 
        для tile-service и time-service.
        Создаём процесс pexpect, переходим в докер onearth, далее исполняем команду.
        """
        
        # Cоздаём процесс и отправляем команду для перехода в onearth
        with pexpect.spawnu(f'ssh {USER_SERVER_OE}') as ssh:
            respond = ssh.expect_exact([PASSWORD_REQUEST_OE, pexpect.EOF, pexpect.TIMEOUT, YES])
            if respond == 3:  # проверяем нужно ли подтверждение
                ssh.sendline('yes')  # отправляем подтверждение 
                ssh.expect('.*')  # ожидаем возврат любой строки
                
            if respond not in (1, 2):  # проверяем нет ли ошибок и учитываем вариант YES 
                ssh.sendline(PASSWORD)  # отправляем пароль
                if ssh.expect_exact([pexpect.EOF, pexpect.TIMEOUT], timeout=1) == 1:  # pexpect.TIMEOUT для подтверждения
                    
                    # Отправляем нашу команду 
                    ssh.sendline(command)
                    respond = ssh.expect_exact([SUDO_PASSWORD_REQUEST, pexpect.EOF, pexpect.TIMEOUT, YES]) 
                    if respond == 3:  # проверяем нужно ли подтверждение
                        ssh.sendline('yes')  # отправляем подтверждение 
                        ssh.expect('.*')  # ожидаем возврат любой строки
                    
                    if respond not in (1, 2):  # проверяем нет ли ошибок и учитываем вариант YES
                        ssh.sendline(PASSWORD)  # отправляем пароль
                        if ssh.expect_exact([pexpect.EOF, pexpect.TIMEOUT], timeout=pause) == 1:  # pexpect.TIMEOUT для подтверждения
                            print('[+] Command:', command)
                            return True

            print(error_message)
            print('Error:', ssh.before)
        return False
    
    
    def _send_command_ssh_worldview_status(self, 
                                           command: str,  # команда которая будет исполняться 
                                           error_message: str,  # сообщение которое будет выведено в случае неверного ответа 
                                           cd_wv: bool=False,  # чтобы переидти в worldview
                                           pause: int=1) -> bool:  # пауза стоит не случайно, иногда команда не успевает исполниться
        """
        Метод нужeн для отправки односложной команды из onearth (например для внесений изменений в конфиг файл).
        Создаём процесс pexpect, переходим в докер worldview, далее исполняем команду.
        """
        
        # Cоздаём процесс и отправляем команду для перехода в worldview
        with pexpect.spawnu(f'ssh {USER_SERVER_WV}') as ssh:
            respond = ssh.expect_exact([PASSWORD_REQUEST_WV, pexpect.EOF, pexpect.TIMEOUT, YES])
            if respond == 3:  # проверяем нужно ли подтверждение
                ssh.sendline('yes')  # отправляем подтверждение 
                ssh.expect('.*')  # ожидаем возврат любой строки
                
            if respond not in (1, 2):  # проверяем нет ли ошибок и учитываем вариант YES 
                ssh.sendline(PASSWORD)  # отправляем пароль
                if ssh.expect_exact([pexpect.EOF, pexpect.TIMEOUT], timeout=1) == 1:  # pexpect.TIMEOUT для подтверждения
                    
                    # Переходим в worldview
                    respond = 1
                    if cd_wv:
                        ssh.sendline('cd worldview')
                        respond = ssh.expect_exact([pexpect.EOF, pexpect.TIMEOUT], timeout=1)
                
                    # Отправляем нашу команду
                    if respond == 1:
                        ssh.sendline(command)
                        if ssh.expect_exact([pexpect.EOF, pexpect.TIMEOUT], timeout=pause) == 1:  # pexpect.TIMEOUT для подтверждения
                            print('[+] Command:', command)
                            return True

            print(error_message)
            print('Error:', ssh.before)    
        return False
        
        
    # Onearth methods   
    # ======================================================================================================================================
    def ssh_onearth_create_dirs(self):
        """
        Метод для создания необходимых директорий. 
        """
        
        # Генерируем команду
        config_command = f'sudo docker exec -it onearth-tile-services mkdir -p /var/www/html/oe-status/{self.layer_id}/default/1km/'
        
        # Исполняем команду
        self._send_command_ssh_onearth_status(command=config_command, 
                                              error_message=f'[!] Error when try to add {config_command.split(" ")[-1]}')
        
        
    # Worldview methods
    # ======================================================================================================================================
    def ssh_worldview_rebuild(self):
        """
        Метод для ребилда worldview.
        """
        
        pause = 333  # обычно ребилд происходит примерно за 4 минуты 
        kill_command = 'pkill -f node'
        build_command = 'npm run build'
        start_command = 'npm start &'
        
        if not self._send_command_ssh_worldview_status(command=kill_command,  # останавливаем worldview 
                                                       error_message='[!] Error when try to kill the worldview',
                                                       cd_wv=True):
            return False
        print(f'[i] Starting to rebuild worldview... waiting for {pause}s...')
        if not self._send_command_ssh_worldview_status(command=build_command,  # билдим worldview
                                                error_message='[!] Error when try to build the worldview',
                                                cd_wv=True,
                                                pause=pause):
            return False
        if not self._send_command_ssh_worldview_status(command=start_command,  # запускаем worldview
                                                error_message='[!] Error when try to start the worldview',
                                                cd_wv=True):
            return False
        return True
    
        
    # Tile-service methods
    # ======================================================================================================================================
    def _ssh_onearth_restart_tile_service(self):
        """
        Метод для перезапуска tile-service.
        """
        
        # Исполняем команду
        self._send_command_ssh_onearth_status(command='sudo docker exec -it onearth-tile-services httpd -k graceful', 
                                              error_message='[!] Error when try to restart tile service')
    
        
    # Time-service methods   
    # ======================================================================================================================================
    def _ssh_onearth_set_time_in_time_service(self, 
                                              f_datetime: datetime):  # дата и время самого раннего снимка
        """
        Метод для добавления временного периода в time-service.
        
        Каким образом задаётся временной отрезок:
            "{YYYY}-{MM}-{DD}T{HH}:{MM}:{ss}Z/{YYYY}-{MM}-{DD}T{HH}:{MM}:{ss}Z/P{period}"
            "2022-04-03T19:45:00Z/2022-04-04T01:00:00Z/PT15M"
        
        Можно ли задавать временной промежуток в будущее? Можно и даже нужно!     
        """
        
        # Задаём начальную и конечную дату/время
        s_date = f_datetime
        e_date = s_date + timedelta(weeks=10428.6)  # через 200 лет придёшь, проверишь как работает
        
        # Определяем постфикс (период времени на протяжении которого будет показываться снимок)
        if self.period < timedelta(minutes=60):
            p = f'T{(self.period.seconds//60)%60}M'  # период в минутах
        elif self.period < timedelta(hours=24):
            p = f'T{self.period.seconds//3600}H'  # период в часах
        else:
            p = f'{self.preiod.days}D'  # период в днях
        
        # Генерируем команду
        time_command = 'sudo docker exec -it onearth-time-service redis-cli sadd layer:{}:periods "{}T{}Z/{}T{}Z/P{}"'.format(
            self.layer_id, 
            s_date.strftime("%Y-%m-%d"), 
            s_date.strftime("%H:%M:%S"), 
            e_date.strftime("%Y-%m-%d"),
            e_date.strftime("%H:%M:%S"), 
            p)
        
        # Исполняем команду
        self._send_command_ssh_onearth_status(command=time_command, 
                                              error_message=f'[!] Error when try to add period to time service')
    
    
    def _ssh_onearth_set_deafult_image_in_time_service(self, 
                                                       f_name: str):  # имя файла, которое содержит дату и время
        """
        Метод для добавление снимка по умолчанию.
        """
        
        date = datetime.strptime(f_name.split('-')[-1], '%Y%j%H%M%S')  # читаем время и дату из имени файла
        
        # Генерируем команду
        default_command = 'sudo docker exec -it onearth-time-service redis-cli set "layer:{}:default" "{}T{}Z"'.format(
            self.layer_id, date.strftime("%Y-%m-%d"), date.strftime("%H:%M:%S"))
        
        # Исполняем команду
        self._send_command_ssh_onearth_status(command=default_command, 
                                              error_message=f'[!] Error when try to add default image to time service')
        
    def _ssh_onearth_restart_time_service(self):
        """
        Метод для перезапуска time-service.
        """
        
        # Исполняем команду
        self._send_command_ssh_onearth_status(command='sudo docker exec -it onearth-time-service httpd -k graceful', 
                                              error_message='[!] Error when try to restart time service')
    
    
    # Configs methods
    # ======================================================================================================================================
    def create_config(self, 
                      f_name: str,  # имя файла
                      text: str) -> str:  # содержимое файла
        """
        Метод для создания текстового файла.
        Создаётся файл, наполняется контентом, возвращается полный путь к файлу.
        """
        
        # Создаём файл
        with open(f_name, 'w') as f:
            f.write(text)
        
        # Если файл создался успешно возвращаем полный путь до файла
        if os.path.exists(f_name):
            return os.path.abspath(f_name)
        
        print(f'[!] Cannot create {f_name} file')
        return ''
    
    
    def move_config(self, 
                    f_path: str,  # полный путь до файла
                    destination: str,  # путь до места назначения
                    clean: bool=True,  # удалять ли файл по пути f_path
                    docker: str=None) -> bool:  # либо capabilities, либо tile-service
        """
        Метод для перемещения конфиг файла в докер onearth или worldview.
        Файл перемещается в докер и, если требуется, из докера перемещается в capabilities или tile-service. 
        """
        
        f_name = os.path.basename(f_path)  # получаем имя файла
        
        # Перемещаем файл на onearth или worldview
        command_file = f'scp {f_path} {USER_SERVER_WV}:/home/{USER}/{destination}{f_name}'
        if docker:  # tile-service только у onearth
            command_file = f'scp {f_path} {USER_SERVER_OE}:/home/{USER}/{f_name}' 
        if not self._send_scp_command_from_nfs_to_ssh_docker(command=command_file, 
                                                             error_message=f'[!] Error when try copy {f_name} to vm',
                                                             password_request=PASSWORD_REQUEST_OE if docker else PASSWORD_REQUEST_WV, 
                                                             file_name=f_name):
            return False
        
        # В случае с onearth нам дополнительно требуется переместить файл в capabilities или tile-service
        if docker:
            command_destination = f'sudo docker exec -it {docker} scp {USER_SERVER_OE}:/home/{USER}/{f_name} {destination}{f_name}'
            if not self._send_command_ssh_onearth_scp(command=command_destination, 
                                                      error_message=f'[!] Error when try to move {f_name} from vm to {destination}{f_name}',
                                                      file_name=f_name):
                return False

            # Удаляем файл из onearth
            command_vm_rm = f'sudo rm {f_name}'
            self._send_command_ssh_onearth_status(command=command_vm_rm,
                                                  error_message=f'[!] Error when try to remove {f_name} from vm')
        
        
        # Удаляем файл из ***
        if clean and os.path.exists(f_name): 
            command_nfs_rm = f'rm {f_name}'
            os.system(command_nfs_rm)

        return True
     
        
    def create_configs(self) -> bool:
        """
        Метод для создания небходимых конфиг файлов для нового слоя.
        """

        # 1. Для GetCapabilities
        # ======================
        gc_conf = f"""layer_id: "{self.layer_id}" 
layer_title: "{self.title}"
layer_name: "{self.subtitle}"
projection: "EPSG:4326"
tilematrixset: "1km"
mime_type: "image/jpeg"
static: false
abstract: "{self.layer_id} abstract"
time_config: "DETECT"
cache_expiration: 1200
metadata:\n""" +\
        '  - {}\n' +\
        f"""source_mrf:
  size_x: {self.size_x}  
  size_y: {self.size_y}
  bands: 3
  tile_size_x: 512
  tile_size_y: 512
  idx_path: "/mnt/nfs/{self.layer_id}"
  data_file_path: "/mnt/nfs/{self.layer_id}"
  year_dir: false
  bbox: -180,-90,180,90
  empty_tile: "{self.empty_tile}" """
    
        c_path = self.create_config(f_name=f'{self.layer_id}.yaml',
                                    text=gc_conf)
        if c_path:
            if not self.move_config(f_path=c_path,
                                    docker='onearth-capabilities',
                                    destination='/etc/onearth/config/layers/oe-status/'):
                print('[!] Unable to move config file to GetCapabilities')
                return False
        else:
            print('[!] Unable to create config file for GetCapabilities')
            return False


        # 2. Для Apache для tile-services 
        # ===============================
        ap_conf = f"""<Directory /var/www/html/oe-status/{self.layer_id}>
    WMTSWrapperRole layer
</Directory>
<Directory /var/www/html/oe-status/{self.layer_id}/default>
        WMTSWrapperRole style
        WMTSWrapperEnableTime On
        WMTSWrapperTimeLookupUri "/oe2-time-service-proxy-onearth-time-service"
</Directory>
<Directory /var/www/html/oe-status/{self.layer_id}/default/1km>
        MRF On
        MRF_ConfigurationFile /var/www/html/oe-status/{self.layer_id}/default/1km/mod_mrf.config
        MRF_RegExp {self.layer_id}
        WMTSWrapperRole tilematrixset
        WMTSWrapperEnableYearDir Off
        WMTSWrapperLayerAlias {self.layer_id}
        WMTSWrapperMimeType image/jpeg
        Header Always Set Cache-Control "public, max-age=1200"
</Directory>"""
    
        c_path = self.create_config(f_name=f'{self.layer_id}.conf',
                                    text=ap_conf)
        if c_path:
            if not self.move_config(f_path=c_path,
                                    docker='onearth-tile-services',
                                    destination='/etc/httpd/conf.d/'):
                print('[!] Unable to move config file to Apache')
                return False
        else:
            print('[!] Unable to create config file for Apache')
            return False


        # 3. Для чтения мрф файлов в tile-service
        # =======================================
        ts_conf = f"""Size 40960 20480 1 3
PageSize 512 512 1 3
SkippedLevels 1
IndexFile /mnt/{self.layer_id}/$""" + '{filename}.idx\n' +\
        f'DataFile /mnt/{self.layer_id}/$' + '{filename}.pjg\n' +\
        f"""EmptyTile {self.empty_tile}"""
    
        c_path = self.create_config(f_name='mod_mrf.config',
                                    text=ts_conf)
        if c_path:
            if not self.move_config(f_path=c_path,
                                    docker='onearth-tile-services',
                                    destination=f'/var/www/html/oe-status/{self.layer_id}/default/1km/'):
                print('[!] Unable to move config file to tile services')
                return False
        else:
            print('[!] Unable to create config file for tile services')
            return False
    
    
        # 4. Добавляем json для нового слоя в worldview
        # =============================================
        wv_data = {'layers':{
            self.layer_id: {
                'id': self.layer_id,
                'title': self.title,
                'subtitle': self.subtitle,
                'description': 'oe2/',  # before: f'oe2/{self.layer_id}'
                'tags': 'bm bmng',
                'group': self.group,
                'layergroup': self.layer_group,
                'wrapX': True, 
                'daynight': ['day']  # what does 'day' means here?
            }}}
        wv_json = json.dumps(wv_data)
        j_path = self.create_config(f_name=f'{self.layer_id}.json',
                                    text=wv_json)
        if j_path:
            if not self.move_config(f_path=j_path, 
                                    destination='worldview/config/default/common/config/wv.json/layers/oe2/'):
                print('[!] Unable to move layer json file to worldview')
                return False
        else:
            print('[!] Unable to create layer json file for worldview')
            return False


        # 5. Добавляем новый идентификатор слоя в worldview json
        # ======================================================
        wv_layer_order_path = 'worldview/config/default/common/config/wv.json/layerOrder.json'
        if not self._send_command_ssh_worldview_status(
            command=fr"""sed -i 's/"layerOrder": \[/"layerOrder": \[ \n\t\t"{self.layer_id}",/' {wv_layer_order_path}""", 
            error_message='[!] Error when try to add layer identifier to worldview json'):
            return False
        
        
        # 6. Добавляем файл описания слоя
        # ===============================
        if os.path.exists(self.description):
              if not self.move_config(f_path=self.description,
                                      destination=f'worldview/config/default/common/config/wv.json/layers/oe2/',
                                      clean=False):
                print('[!] Unable to move description file to the worldview')
                return False
        else:
            print('[!] File with layer description does not exist')
            
            
        # 7. Создаём новую категорию
        # ==========================
        if self.create_nc:
            cat_json = json.dumps(self.create_new_category)
            cj_path = self.create_config(f_name=f'{self.new_category_id}.json',
                                         text=cat_json)
            if cj_path:
                category = list(self.create_new_category['categories'].keys())[0]
                if not self.move_config(f_path=cj_path,  
                                        destination=f'worldview/config/default/common/config/wv.json/categories/{self.category_in}/'):
                    print('[!] Unable to move new category json file to worldview')
                    return False
            else:
                print('[!] Unable to create new category json file for worldview')
                return False
        
        
        # 8. Создаём новую группу
        # =======================        
        if self.create_ng:
            group_json = json.dumps(self.create_new_group)
            gj_path = self.create_config(f_name=f'{self.layer_id}.json',
                                         text=group_json)
            if gj_path:
                if not self.move_config(f_path=gj_path,  
                                        destination='worldview/config/default/common/config/wv.json/measurements/'):
                    print('[!] Unable to move new group json file to worldview')
                    return False
            else:
                print('[!] Unable to create new group json file for worldview')
                return False
            
            
            # 8.1 Добавляем новую группу в категорию
            # ======================================
            if not self._send_command_ssh_worldview_status(
                command=fr"""sed -i 's/"measurements": \[/"measurements": \[ \n\t\t\t\t"{self.group}",/' {self.add_new_group_to}""", 
                error_message='[!] Error when try to add layer identifier to worldview json'):
                return False 
        
        return True    

    
    # Run
    # ====================================================================================================================================== 
    def run(self,
            f_names: list):  # cписок всех имён мрф файлов 
        """
        Метод для добавления временных периодов и перезапуска time-service и tile-service
        """
        
        # Перезапускаем time-service
        self._ssh_onearth_restart_tile_service()
        
        # Добавляем временные период на много лет вперёд
        first_datetime = datetime.strptime(sorted(f_names)[0].split('-')[-1], '%Y%j%H%M%S')
        self._ssh_onearth_set_time_in_time_service(first_datetime)  
        
        # Устанавливаем снимок по умолчанию и перезапускаем time-service
        self._ssh_onearth_set_deafult_image_in_time_service(f_names[-1])
        self._ssh_onearth_restart_time_service() 


# ------------------------------------------------------------------------------------------------------------------------------------------
def get_folders_with_mrf_files(path: str=MAIN_PATH) -> list:  # путь до подмонтированной директории
    """
    Функция для считывания имён всех папок в path. 
    """
    
    folders_names = []
    for element in glob(path + '*'):
        if not os.path.isfile(element):
            folders_names.append(os.path.basename(element))

    return folders_names


# ------------------------------------------------------------------------------------------------------------------------------------------
def get_existing_layers_list(path: str=MAIN_PATH,  # путь до подмонтированной директории
                             f_name: str=LAYERS_FILE) -> list:  # название текстового файла, где храняться добавленные слои
    """
    Функция для считывания всех добавленных слоёв в файле f_name.
    """
    
    layers_list = []
    with open(path + f_name) as f:
        for line in f.readlines():
            layers_list.append(line.strip())

    return layers_list


# ------------------------------------------------------------------------------------------------------------------------------------------
def main():
    """
    Функция которая читает все папки, сравнивает их со списком уже добавленных слоёв. 
    Если появилась новая папка происходит проверка на наличие конфиг файла и наличие пар .idx и .pjg.
    После происходит создание конфиг файлов, добваление временных периодов, перезапуск сервисов и ребилд worldview.
    """
    
    status_added = False
    
    # Для каждой папки из списка
    for folder in get_folders_with_mrf_files():
        if folder in get_existing_layers_list():  # сверка со списком 
            continue

        # Пустая ли папка
        if not os.listdir(MAIN_PATH + folder):
            print(f'[!] {folder} folder is empty')
            continue

        # Проверка наличия конфиг файла
        metadata = {}
        try:
            config_paths = glob(MAIN_PATH + folder + '/*.json')
            if not config_paths:
                raise Exception(f'[!] There is no config file in {folder} folder')
                
            with open(config_paths[0], 'r') as f:
                metadata = json.load(f)
                if not metadata:
                    raise Exception(f'[!] Config file is empty in {folder} folder')
                
        except Exception as ex:
            print(ex)
            continue
            

        # Проверка пар .idx и .pjg и получение всех имён этих файлов
        data = []
        try: 
            idxes = sorted(glob(MAIN_PATH + folder + '/*.idx'))
            pjgs = sorted(glob(MAIN_PATH + folder + '/*.pjg'))

            if len(idxes) != len(pjgs):
                raise Exception(f'[!] Some of .idx or .pjg do not have a pair in {folder} folder') 

            for idx, pjg in zip(idxes, pjgs):
                label_idx = os.path.basename(idx).split('.')[0]
                label_pjg = os.path.basename(pjg).split('.')[0]
                if label_idx != label_pjg:
                    raise Exception(f'[!] Some of .idx or .pjg pair names do not match in {folder} folder') 

                data.append(label_idx) 

        except Exception as ex:
            print(ex)
            continue
            
        try:
            pub = Publisher(**metadata)
        except Exception as ex:
            print(f'[!] Config file is wrong in the {folder} folder, plz take the right example on the wiki')
            print('Esception:', ex)
            continue
        
        # Создаём директории
        pub.ssh_onearth_create_dirs()
        
        # Создаём конфиги
        if pub.create_configs():
            try:
                # Добавляем идентификатор нового слоя в список существующих слоёв
                with open(MAIN_PATH + LAYERS_FILE, 'a') as f:
                    f.write(metadata['identifier'] + '\n')
                    print(f"[+] {metadata['identifier']} added to existing layers")
            except Exception as ex:
                print(f"[!] Unable to save new {metadata['identifier']} layer to {LAYERS_FILE}")
            
            # Перезапускаем сервисы и добавляем временные отрезки
            pub.run(data)
            status_added = True
        else:
            print(f'[!] Unable to create config files for {folder}')
    
    if status_added:
        p = Publisher(0, 0, '0', '0', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)  # да, некрасиво
        p.ssh_worldview_rebuild()
        
# ------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__': 
    main()
    