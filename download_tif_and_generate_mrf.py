import os
import sys
import dask
import pexpect
import requests
from time import sleep
from datetime import datetime

SAVE_PATH = '/***'
PASSWORD = '12345678'

# Loader class
# ==========================================================================================================================
class ETRIS_Loader:
    # --------------------------------------------------
    def __init__(self, date: datetime, is_arctic=True):
        self.satellite_name = 'Arctic-M' if is_arctic else 'Electro-L3'
        self.abbreviation = 'ARCM1' if is_arctic else 'EL3'
        self.date = date
        self.closest_available_date = None
        
        
    # ----------------------------------------    
    def get_available_dates_list(self) -> list:
        """
        Метод возравращает список всех доступных данных с учётом временного периода, в данном случае за определённый день.
        Т.е. если у нас self.data = datetime(2022, 5, 23, 4, 15), то список доступных данных будет сформирован для 23.05.2022.
        """
        
        dates = []
        
        # Для формирования списка используется примитивный алгоритм:
        #     Генерируем ссылку для каждых пяти минут, и на основе status_code, который получаем от requests.get,
        #     добавляем в список с результатами.
        #     Иногда данные доступны с пропусками (например отсутсвуют данные с 00:00 до 3:15). 
        for hour in range(0, 24):  # для каждого часа
            for minute in range(0, 60, 5):  # для каждой минуты
                with requests.get('https://s3ext.gptl.ru/stac-web-free/{name}/{year}/{month:02d}/'
                                  'ETRIS.{abbreviation}.{year}-{month:02d}-{day:02d}T{hour:02d}{minute:02d}.L2.3857/'
                                  'ETRIS.{abbreviation}.{year}-{month:02d}-{day:02d}T{hour:02d}{minute:02d}.L2.3857.tif'.
                                  format(year=self.date.year, 
                                         month=self.date.month, 
                                         day=self.date.day, 
                                         hour=hour, 
                                         minute=minute,
                                         name=self.satellite_name,
                                         abbreviation=self.abbreviation)) as r:
                    
                    if r.status_code == 200:
                        dt = (self.date.year, self.date.month, self.date.day, hour, minute)
                        print('Data available for: {}-{:02d}-{:02d}T{:02d}{:02d}'.format(*dt)) 
                        dates.append(datetime(*dt))
        if dates:
            self.closest_available_date = min(dates, key=lambda x: abs(x - self.date))  # находим ближайшее время к нашему исходному времени
            
        return dates
    
    
    # -------------------------------------------
    def load_data(self, l_date: datetime) -> str:
        """
        На вход получем дату для СУЩЕСТВУЮЩИХ ДАННЫХ.
        На выход возвращаем путь уже скаченного файла, который позже понадобится для MRF генератора.
        """
        
        loaded = None
        status = None
        attempts = 3  # Три попытки, так как мы знаем что данные существуют
        while attempts > 0:
            try:
                link = """
                       https://s3ext.gptl.ru/stac-web-free/{name}/{year}/{month:02d}/
                       ETRIS.{abbreviation}.{year}-{month:02d}-{day:02d}T{hour:02d}{minute:02d}.L2.3857/
                       ETRIS.{abbreviation}.{year}-{month:02d}-{day:02d}T{hour:02d}{minute:02d}.L2.3857.tif
                       """.format(year=l_date.year, 
                                  month=l_date.month, 
                                  day=l_date.day, 
                                  hour=l_date.hour, 
                                  minute=l_date.minute,
                                  name=self.satellite_name,
                                  abbreviation=self.abbreviation).replace('\n', '').replace(' ', '')
                
                with requests.get(link) as r:
                    status = r.status_code
                    download_dir = os.path.join(SAVE_PATH, 
                                                self.satellite_name, 
                                                str(l_date.year),
                                                '{:02d}'.format(l_date.month),
                                                link.split('/')[-2])
                    
                    if not os.path.isdir(download_dir):
                        os.makedirs(download_dir)
                    
                    d_path = download_dir + '/' + link.split('/')[-1]
                    with open(d_path, 'wb') as w:
                        w.write(r.content)
                        loaded = d_path
                        print('[+]' + d_path)
                        break
                        
            except Exception as ex:
                print(f'[!]{l_date.strftime("%Y-%m-%dT%H%M")} Exception: {ex}, Status code: {status}')
                sleep(1)
            attempts -= 1
    
        return loaded
    
    
    # --------------------
    def run_unknown(self):
        """
        Получаем доступные данные за день и скачиваем только ближайший доступный tif.
        """
        
        self.get_available_dates_list()  # получем все доступные данные за определённый день
        if self.closest_available_date:
            return self.load_data(self.closest_available_date)  # скачиваем ближайший (по времени) доступный tif 

        
# ==========================================================================================================================
def load_tif(exact_dates: list=None, 
             exact_date: datetime=None, 
             unknown_date: datetime=None, 
             load_whole_day: bool=False, 
             is_arctic: bool=True) -> list:  # у нас только два возможных варианта  
    """
    Четыре варианта для скачивания:
        1. (exact_dates)    На вход получаем список с уже извесными корректными датами, и скачиваем весь список.
        2. (exact_date)     На вход получаем одну корректную дату, и скачиваем tif файл.
        3. (unknown_date,   На вход получаем одну дату и положительный флаг, и скачиваем все доступные данные с этой даты. 
            load_whole_day=True)  
        4. (unknown_date,   На вход получаем одну дату и отрицательный флаг, и скачиваем лишь ближайший (по времени, за эту дату) доступный tif файл. 
            load_whole_day=False) 
    На выход возвращаем список скаченных tif файлов.
    """
    
    if exact_dates:
        dask_objects = []
        for dt in exact_dates:
            loader = ETRIS_Loader(dt, is_arctic=is_arctic)
            dask_objects.append(dask.delayed(loader.load_data)(dt))

        return dask.delayed(dask_objects).compute()
    elif exact_date:
        return [ETRIS_Loader(exact_date, is_arctic=is_arctic).load_data(exact_date)]
    elif unknown_date and not load_whole_day:
        return [ETRIS_Loader(unknown_date, is_arctic=is_arctic).run_unknown()]
    elif unknown_date and load_whole_day:
        loader = ETRIS_Loader(unknown_date, is_arctic=is_arctic)
        available_dates = loader.get_available_dates_list()
        if available_dates:
            dask_objects = []
            for date in available_dates:
                dask_objects.append(dask.delayed(loader.load_data)(date))
            return dask.delayed(dask_objects).compute()
    
    return []
   
    
# ==========================================================================================================================
def generate_mrf(f_path: str):
    """
    На вход мы получаем полный путь до tif файла: 
        /***/ETRIS.EL3.2022-05-26T0000.L2.3857/ETRIS.EL3.2022-05-26T0000.L2.3857.tif
        имеет значение только часть ETRIS.EL3.2022-05-26T0000.L2.3857/ETRIS.EL3.2022-05-26T0000.L2.3857.tif, 
        важно придерживаться именно такой структуры для корректной работы mrfgen приложения.
    """
    
    # Генерируем команду которая запускает mrfgen в докере
    command = f'sudo docker run -v /***:/*** mrfgen {os.path.dirname(f_path)}'
    
    # Создаём скрытый процесс через pexpect, который в свою очередь исполняет наши консольные команды
    # Обращаю внимание, что если мы меняем сервера, то и команды (в данном случае формат запроса паролей) могут меняться!
    with pexpect.spawnu('su user') as p:
        status = p.expect_exact('Password: ')  
        if status == 0:
            p.sendline(PASSWORD)
            p.expect('\$')
            p.sendline(command)
            print(p.expect_exact('[sudo] password for user: '))  
            p.sendline(PASSWORD)
            sleep(300)  # тут я использую фиксированное время ожидания в 5 минут, чтобы дать процессу закончить генерацию mrf файлов
            print(f'[i] {command} finished...') 


# =========================================================================================================================
def convert(paths: list, create_file: bool=True):
    """
    Генерация mrf файлов в режиме многопроцессорности
    """
    
    dask_objects = []
    for path in paths:
        dask_objects.append(dask.delayed(generate_mrf)(path))

    dask.delayed(dask_objects).compute()
    
            
# ==========================================================================================================================            
if __name__ == '__main__':
    
    # Примеры использования
    
    # Arktic-M
    # ========
    exact_time_list = [datetime(2022, 5, 30, 4, 15), 
                       datetime(2022, 5, 29, 19, 15),
                       datetime(2022, 5, 28, 3, 30)]
    exact_time = datetime(2022, 5, 31, 4, 15)
    
    # Скачиваем tifы
#     arctic_exact_list = load_tif(exact_dates=exact_time_list)
#     arctic_exact = load_tif(exact_date=exact_time)
#     arctic_unknown = load_tif(unknown_date=datetime(2022, 6, 1, 22, 33))
    arctic_whole_day = load_tif(unknown_date=datetime(2022, 6, 13, 0, 0), load_whole_day=True)
    
    arctic_tifs_paths = arctic_whole_day
    
    # Конверотим tif в mrf
    convert(arctic_tifs_paths)
    
#     # Electro-L
#     # =========
#     exact_time_list = [datetime(2022, 5, 30, 0, 0), 
#                        datetime(2022, 5, 29, 0, 0),
#                        datetime(2022, 5, 28, 0, 0)]
#     exact_time = datetime(2022, 5, 31, 0, 0)
    
#     # Скачиваем tifы
#     electro_exact_list = load_tif(exact_dates=exact_time_list, is_arctic=False)
#     electro_exact = load_tif(exact_date=exact_time, is_arctic=False)
#     electro_unknown = load_tif(unknown_date=datetime(2022, 6, 1, 22, 33), is_arctic=False)
#     electro_whole_day = load_tif(unknown_date=datetime(2022, 6, 15, 0, 0), load_whole_day=True, is_arctic=False)
#     electro_tifs_paths = electro_whole_day 
    
#     # Конверотим tif в mrf
#     convert(electro_tifs_paths)

    